!(function($) {
    timerVars = {};
    $(document).ready(function() {
        /*---- ON PAGE READY ---*/
        window.onresize = function(event) {
            flexTable();
        };
        // DISPLACE TABLE HEADER
        $(".table-responsive-stack")
            .find("th")
            .each(function(i) {
                $(
                    ".table-responsive-stack td:nth-child(" + (i + 1) + ")"
                ).prepend(
                    '<span class="table-responsive-stack-thead">' +
                    $(this).text() +
                    "</span> "
                );
            });
        flexTable();

        // SET TIMER
        $("button.set-timer").on("click", ajaxHandler);
        // SET TIMER ON PAGE LOAD
        $.each($("button.set-timer"), function() {
            var taskId = $(this).data("target"),
                btn = $(this);
            if (btn.hasClass("timer-on")) {
                // disable all other tasks
                $("button.set-timer:not([data-target='" + taskId + "'])").attr(
                    "disabled",
                    ""
                );
                // set timer task
                timerVars[taskId] = setInterval(() => {
                    setTimer(btn);
                }, 1000);
            }
        });

        /* ---- FUNCTIONS ---*/
        // STACK TASKS TABLE
        function flexTable() {
            if ($(window).width() < 768) {
                $(".table-responsive-stack").each(function(i) {
                    $(this)
                        .find(".table-responsive-stack-thead")
                        .show();
                    $(this)
                        .find("thead")
                        .hide();
                });
            } else {
                $(".table-responsive-stack").each(function(i) {
                    $(this)
                        .find(".table-responsive-stack-thead")
                        .hide();
                    $(this)
                        .find("thead")
                        .show();
                });
            }
        }
        // SET TIMER AJAX HANDLER
        function ajaxHandler() {
            var taskId = $(this).data("target"),
                btn = $(this),
                otherBtns = $(
                    "button.set-timer:not([data-target='" + taskId + "'])"
                );
            if (!otherBtns.hasClass("timer-on")) {
                // clear previous timer
                clearInterval(timerVars[taskId]);
                // setup ajax request
                $.ajax({
                    type: "PUT",
                    url:
                        "/set-timer/" +
                        taskId +
                        "?timer_current=" +
                        $.trim(btn.html()),
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    success: function(data) {
                        if (data.start === true) {
                            // disable all other tasks
                            $(
                                "button.set-timer:not([data-target='" +
                                taskId +
                                "'])"
                            ).attr("disabled", "");
                            // change task status
                            btn.removeClass("btn-primary timer-off");
                            btn.addClass("btn-danger timer-on");
                            // set timer task
                            timerVars[taskId] = setInterval(() => {
                                setTimer(btn);
                            }, 1000);
                        } else {
                            // enable all other tasks
                            $("button.set-timer").removeAttr("disabled");
                            // change task status
                            btn.removeClass("btn-danger timer-on");
                            btn.addClass("btn-primary timer-off");
                        }
                    }
                });
            }
        }
        // SET TIMER HELPER. UPDATES TIMER EVERY SECOND
        function setTimer(btn) {
            var date = $.trim(btn.html()).split(":"),
                hours = parseInt(date[0]),
                minutes = parseInt(date[1]),
                seconds = parseInt(date[2]);

            seconds += 1;
            if (seconds >= 60) {
                seconds = 0;
                minutes += 1;
                if (minutes >= 60) {
                    minutes = 0;
                    hours += 1;
                }
            }
            var newDate =
                hours.toString().padStart(2, "0") +
                ":" +
                minutes.toString().padStart(2, "0") +
                ":" +
                seconds.toString().padStart(2, "0");

            btn.html(newDate);
        }
    });
})(window.jQuery);