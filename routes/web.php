<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',     ['as' => 'tasks.index', 'uses' => 'TaskController@index']);
Route::post('/',    ['as' => 'tasks.add',   'uses' => 'TaskController@addTask']);

//AJAX
Route::put('set-timer/{task}', ['as' => 'ajax.setTimer', 'uses' => 'TaskController@setTimer']);