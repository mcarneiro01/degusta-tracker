<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    protected $users = [
        [
            'name'      => 'degusta_box',
            'email'     => 'admin@admin',
            'password'  => '%q=aL3R2AbNZJD?f',
        ]
    ];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users as $user) {
            User::updateOrCreate([
                'email'     => $user['email'],
            ], [
                'name'      => $user['name'],
                'password'  => Hash::make($user['password']),
            ]);
        }
    }
}