<?php

namespace App\Http\Controllers;

use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    protected $folder = 'tasks';
    public function index(Request $request)
    {
        $folder = $this->folder;
        // No login middleware
        $user = Auth::loginUsingId(1);
        // get tasks assigned to user
        $tasks = $user->tasks();
        // filter tasks by search input
        if ($request->search) {
            $tasks->search(['title', 'description'], $request->search);
        }
        $tasks = $tasks->get();
        // are there any tasks already on
        $task_on = $user->tasks()->whereNotNull('timer_start')->first();

        return view($this->folder . '.index', compact('folder', 'tasks', 'request', 'task_on'));
    }
    public function addTask(Request $request)
    {
        // No login middleware
        $user = Auth::loginUsingId(1);

        // validate input data
        $v = Validator::make($request->all(), [
            'title'         => 'required|max:255',
            'description'   => 'max:600',
        ]);
        if ($v->fails())
            return back()->withErrors($v)->withInput();
        //prepare it to be stored
        $request['user_id'] = $user->id;
        unset($request['_token']);
        // store Task
        Task::create($request->all());

        return back()->with('success', 'Task created successfuly');
    }
    public function setTimer(Task $task, Request $request)
    {
        // set global start
        if(!$task->start)
            $task->start =  Carbon::now()->format('Y-m-d H:i:s');

        $start = true;
        // Timer set workflow
        if (!$task->timer_start)
            $task->timer_start = Carbon::now()->format('Y-m-d H:i:s');
        else {
            $start = false;
            // If query parameter is not defined, time spent won't be lost
            if ($timer_current = $request->query('timer_current')) {
                $timer_current = explode(':', $timer_current);
                $hours          = $timer_current[0] * 3600;
                $minutes        = $timer_current[1] * 60;
                $seconds        = $hours + $minutes + $timer_current[2];

                $task->timer_current = $seconds;
            } else
                $task->timer_current += Carbon::parse($task->timer_start)->diffInSeconds(Carbon::now()->format('H:i:s'));

            $task->timer_start = null;
        }
        $task->save();
        return response()->json(['start' => $start]);
    }
}