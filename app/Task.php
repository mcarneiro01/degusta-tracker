<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title', 'description', 'user_id', 'start', 'timer_start', 'timer_end', 'timer_current'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}