<?php

namespace App\Console\Commands;

use App\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RunTask extends Command
{
    protected $signature = 'task {task_id=0} {action=start} {--list}';
    protected $description = 'Execute an action on a task';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $args = $this->arguments();
        try {
            $task = Task::findOrFail($args['task_id']);
        } catch (\Throwable $th) {
            if ($args['task_id'] != 0)
                return $this->error("Task not found");
        }
        switch ($args['action']) {
            case 'start':
                if (isset($task)) {
                    if (!$task->start)
                        $task->start = Carbon::now()->format('Y-m-d H:i:s');

                    if (!$task->timer_start) {
                        $task->timer_start = Carbon::now()->format('Y-m-d H:i:s');
                        $task->save();
                    }
                }
                break;
            case 'stop':
                if (isset($task) && $task->timer_start) {
                    $task->timer_current += Carbon::parse($task->timer_start)->diffInSeconds(Carbon::now()->format('H:i:s'));;
                    $task->timer_start    = null;
                    $task->save();
                }
                break;
            default:
                return $this->error('command "task {task_id} {action} must have a valid action ("start/stop")');
        }
        if ($this->option('list')) {
            $tasks_table = [];
            $tasks = Task::all();
            $headers = ['ID', 'Title', 'User', 'Start', 'Running', 'Elapsed'];

            foreach ($tasks as $task) {

                if ($task->timer_start) {
                    $task->timer_current += Carbon::parse($task->timer_start)->diffInSeconds(Carbon::now()->format('H:i:s'));
                    $running =  'Yes';
                } else
                    $running = 'No';

                $elapsed = floor($task->timer_current / 3600) . gmdate(":i:s", $task->timer_current % 3600);
                $tasks_table[] = [
                    $task->id,
                    $task->title,
                    $task->user_id,
                    $task->start,
                    $running,
                    $elapsed,
                ];
            }
            $this->table($headers, $tasks_table);
        }
    }
}
