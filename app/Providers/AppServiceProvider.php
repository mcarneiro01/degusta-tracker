<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Search method to look on each individual properties
        Builder::macro('search', function ($attributes, string $search_terms) {
            $this->where(function (Builder $q) use ($attributes, $search_terms) {
                foreach ($attributes as $attribute) {
                    $q->orWhere(function ($q) use ($attribute, $search_terms) {
                        foreach (explode(' ', $search_terms) as $search_term) {
                            $q->where($attribute, 'LIKE', "%$search_term%");
                        }
                    });
                }
            });
            return $this;
        });
    }
}