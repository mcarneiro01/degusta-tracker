**DegustaBox tracker**

---
(In the case of Laravel being runned locally)

## Add dependencies

1. Execute command $ composer update
 
---

## Configure DB

1. .env file in the root directory it is possible to find the config variables for MySql connection. The database is "dgt". To add the .env file simply change filename of .env.example for .env 
2. To add the necessary tables execute the commands below
3. $ php artisan migrate 
4. $ php artisan db:seed

---

## Execute php scripts

The php script is  

1. $ php artisan task {task_id} {action} {--list}

**task_id** is the id of the task in the DB;
**action** can be start/stop
**--list** is the option to display a list of tasks


## Docker Image

The Docker image can be found in https://hub.docker.com/r/mikept01/degusta-tracker/tags