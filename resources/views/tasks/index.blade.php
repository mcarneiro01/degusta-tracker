@extends('base')
@section('content')
    <div class="content">
        @if (\Session::has('success'))
            <div class="msg success">
                {{ \Session::get('success') }}
            </div>
        @endif
        <div class="col-12">

            <div class="row title m-b-md">
                Tasks
            </div>
        </div>
        <div class="row search-tasks">
            <div class="container">
                {{ Form::open(['url' => route("$folder.index"), 'method' => 'GET']) }}
                <div class="col-12">
                    <div class="form-group row">
                        <div class="col-md-6 form-element">
                            {{ Form::text('search', isset($request, $request->search) ? $request->search : null, ['class' => 'form-control', 'placeholder' => 'Search for task...']) }}
                        </div>
                        <div class="col-md-3 form-element">
                            {{ Form::submit('Search', ['class' => 'form-control search-submit']) }}
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <div class="container">
            <div class="row tasks-table">
                <div class="col-lg-12">
                    <table class="table table-bordered table-striped table-responsive-stack">
                        <thead class="thead-dark">
                        <tr>
                            <th>Task</th>
                            <th>Description</th>
                            <th class="timer-header">Time spent </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($tasks as $task)
                            <tr>
                                <td>{{ $task->title }}</td>
                                <td>{{ $task->description }}</td>
                                <td>
                                    @if ($task->timer_start)
                                        <button type="button" class="btn btn-danger set-timer timer-on"
                                                data-target="{{ $task->id }}">
                                            @php
                                                $task->timer_current +=
                                                \Carbon\Carbon::parse($task->timer_start)->diffInSeconds(\Carbon\Carbon::now()->format('H:i:s'))
                                            @endphp
                                            {{  floor($task->timer_current / 3600) . gmdate(":i:s", $task->timer_current % 3600) }}
                                        </button>
                                    @else
                                        <button type="button" @if ($task_on) disabled="" @endif class="btn btn-primary set-timer timer-off"
                                                data-target="{{ $task->id }}">
                                            {{  floor($task->timer_current / 3600) . gmdate(":i:s", $task->timer_current % 3600) }}
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- <div class="paginate">
                    {{ $tasks->links() }}
            </div> --}}
            </div>
            <div class="row add-tasks">
                {{ Form::open(['url' => route("$folder.add"), 'method' => 'POST']) }}
                <div class="col-12">
                    <div>
                        <h4>
                            <span class="title-add">add tasks</span>
                        </h4>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4 form-element">
                            {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Task title']) }}
                            @if ($msg = $errors->first('title'))
                                <div class="msg error"><span>{{ $msg }}</span></div>
                            @endif
                        </div>
                        <div class="col-md-5 form-element">
                            {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Task description', 'rows' => 3  ]) }}
                        </div>
                        <div class="col-md-3 form-element">
                            {{ Form::submit('Add', ['class' => 'form-control search-submit']) }}
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection